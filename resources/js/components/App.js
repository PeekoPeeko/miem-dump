import React from 'react';
import ReactDOM from 'react-dom';

import {fetchAll} from "../clients/miemClient";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            miems: [],
            page: 1,
            lastPage: 1
        };

        this.mapMiems = this.mapMiems.bind(this);
        this.fetchMiems = this.fetchMiems.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
    }

    componentDidMount() {
        this.fetchMiems();

        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll(e) {
        if (window.scrollY > document.body.clientHeight) {
            this.fetchMiems()
        }
    }

    fetchMiems() {
        const {page, miems, lastPage} = this.state;

        if (page <= lastPage) {
            fetchAll(page).then(res => {
                this.setState({
                    miems: [...miems, ...res.data],
                    page: page + 1,
                    lastPage: res.last_page
                })
            }).catch(err => console.log(err));
        }
    }

    mapMiems() {
        const {miems} = this.state;

        return miems.map(miem => (
            <div className="card bg-antrisite text-white mt-5" key={miem.uuid}>
                <div className="card-body">
                    <h4 className="card-title">{miem.name}</h4>
                </div>
                <img className="card-img-bottom" src={miem.file_url} />
            </div>
        ));
    }

    render() {
        const miemsMapped = this.mapMiems();

        return (
            <div className="container">
                {miemsMapped}
            </div>
        );
    }
}

export default App;

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
