import Axios from "axios";

export const fetchAll = (page = 1) => {
    return new Promise((resolve, reject) => {
        Axios.get(`/api/file?page=${page}`).then(res => {
            resolve(res.data);
        }).catch(err => {
            reject(err)
        });
    })
};
