@extends('layouts.base')
@section('content')
    <div class="col-8 mb-5">
        <div id="app"></div>
    </div>
    <div class="add-button">
        <a class="btn btn-outline-dark btn-lg" href="/upload">
            Upload
        </a>
    </div>
@endsection
