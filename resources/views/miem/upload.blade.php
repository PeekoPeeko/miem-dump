@extends('layouts.base')

@section('content')
    <div class="col mt-5">
        <div class="card bg-white">
            <div class="card-body">
                <form id="miemForm">
                    <div class="form-group">
                        <label for="nameInput">Name</label>
                        <input type="text" class="form-control" id="nameInput">
                    </div>
                    <div class="form-group">
                        <label for="miemInput">Miem</label>
                        <input accept="image/*" type="file" class="form-control" id="miemInput">
                    </div>
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <div class="add-button">
        <a class="btn btn-outline-dark btn-lg" href="/">
            Back
        </a>
    </div>
@endsection
