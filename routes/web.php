<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MiemController@index');
Route::get('/upload', 'MiemController@add');

Route::prefix('api')->group(function () {
    Route::prefix('file')->group(function () {
        Route::get('', 'api\FileController@index');
        Route::post('', 'api\FileController@store');
        Route::get('/{uuid}', 'api\FileController@show');
        Route::post('/{uuid}', 'api\FileController@update');
        Route::delete('/{uuid}', 'api\FileController@delete');
    });
});
